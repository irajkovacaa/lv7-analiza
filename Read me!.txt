Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj laboratorijskoj vježbi. 
Omogućiti pokretanje igre, unos imena dvaju igrača, ispis koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na odgovarajućim mjestima 
te ispis dijaloga s porukom o pobjedi, odnosno neriješenom rezultatu kao i praćenje ukupnog rezultata.